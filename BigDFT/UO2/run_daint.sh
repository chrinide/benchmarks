#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit -1
fi

if [ -z "$1" ]
  then
    echo "No number of nodes supplied"
    exit -1
fi

NODES=$1


if [ -z "$2" ]
  then
    echo "No walltime supplied - using 1h"
    TIME="01:00:00"
  else
    TIME=$2
fi

if [ -z "$3" ]
  then
    echo "No name supplied - using default name"
    NAME="bigdft_${NODES}"
  else
    NAME=$3
fi

if [ -z "$4" ]
  then
    echo "No build name supplied - using default name"
    BUILD="build"
  else
    BUILD=$4
fi


if [ -z "$5" ]
  then
    echo "No input file name supplied - using default name"
    INPUT="input"
    NAMEOPTION=""
  else
    INPUT=$5
    NAMEOPTION="--name=${INPUT}"
fi

TMPFILE=$(mktemp -u "$1_XXXXXXXXXX" 2> /dev/null)

FOLDER=${NAME}_${NODES}

if [ -d "$FOLDER" ]
then 
  echo "folder $FOLDER already exists - exiting"
  exit 0
fi

mkdir $FOLDER
cp *.yaml ${FOLDER}
cp *.xyz ${FOLDER}
cp psppar.* ${FOLDER}

if [ -z "$6" ]
  then
    echo "No number of tasks per node specified, use 1"
    NTASKSPN=1
    NTASKS=$NODES
  else
    NTASKSPN=$6
    NTASKS=$(($NODES*$NTASKSPN))
fi

if [ -z "$7" ]
  then
    echo "No number of omp per rank specified, use 12"
    OMPNT=12
  else
    OMPNT=$7
fi



echo "Writing file ${TMPFILE}, with ${NODES} nodes, for job ${NAME} with ${NTASKS} tasks (${NTASKSPN} per node) and ${OMPNT} cores per task"

echo "#!/bin/bash -l
#SBATCH --account=s707
#SBATCH --nodes=${NODES}
#SBATCH --ntasks=${NTASKS}               # -n
#SBATCH --ntasks-per-node=${NTASKSPN}      # -N
#SBATCH --cpus-per-task=${OMPNT}       # -d
##SBATCH --ntasks-per-core=1 # -j    # --ntasks-per-core / -j
#SBATCH --time=${TIME}
#SBATCH --job-name='${NAME}.${NODES}'
#SBATCH --output=${FOLDER}/o_${NAME}.${NODES}.${NTASKSPN}.${OMPNT}.1.daint
#SBATCH --error=${FOLDER}/e_${NAME}.${NODES}.${NTASKSPN}.${OMPNT}.1.daint
#SBATCH -C gpu
echo '# -----------------------------------------------'
#ulimit -c unlimited
ulimit -s unlimited
ulimit -a |awk '{print \"# \"$0}'
echo '# -----------------------------------------------'

echo '# -----------------------------------------------'
# The distribution of MPI tasks on the nodes can be written to the standard output file by setting environment variable 
export CRAY_CUDA_MPS=1
export LD_LIBRARY_PATH=/opt/nvidia/cudatoolkit8.0/8.0.44_GA_2.2.7_g4a6c213-2.1/lib64:/opt/intel/compilers_and_libraries_2017.0.098/linux/mkl/lib/intel64:/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64:${LD_LIBRARY_PATH}
export MALLOC_MMAP_MAX_=0
export MALLOC_TRIM_THRESHOLD_=536870912
export OMP_NUM_THREADS=${OMPNT}
export MPICH_VERSION_DISPLAY=1
export MPICH_ENV_DISPLAY=1
#export MPICH_DBG_FILENAME='log/dbg-%w-%d.log'
#export MPICH_DBG_CLASS=ALL    
#export MPICH_DBG_LEVEL=VERBOSE
export MPICH_NO_BUFFER_ALIAS_CHECK=1

export MPICH_RDMA_ENABLED_CUDA=1

echo '# -----------------------------------------------'

echo '# -----------------------------------------------'
echo '# SLURM_JOB_NODELIST = $SLURM_JOB_NODELIST'
echo '# submit command : '
grep aprun ${TMPFILE}
echo '# SLURM_JOB_NUM_NODES = $SLURM_JOB_NUM_NODES'
echo '# SLURM_JOB_ID = $SLURM_JOB_ID'
echo '# SLURM_JOBID = $SLURM_JOBID'
echo '# SLURM_NTASKS = $SLURM_NTASKS / -n --ntasks'
echo '# SLURM_NTASKS_PER_NODE = $SLURM_NTASKS_PER_NODE / -N --ntasks-per-node'
echo '# SLURM_CPUS_PER_TASK = $SLURM_CPUS_PER_TASK / -d --cpus-per-task'
echo '# OMP_NUM_THREADS = $OMP_NUM_THREADS / -d '
echo '# SLURM_NTASKS_PER_CORE = $SLURM_NTASKS_PER_CORE / -j1 --ntasks-per-core'
echo '# -----------------------------------------------'

date
set +x

unset COMPUTE_PROFILE 
export PMI_NO_FORK=1 

mkdir ${FOLDER}
cp ${INPUT}.yaml ${FOLDER}
cp ${INPUT}.xyz ${FOLDER}
cd ${FOLDER}

srun -N ${NODES} -n ${NTASKS} -c ${OMPNT} ${BUILD} ${NAMEOPTION}


">${TMPFILE}

sbatch ${TMPFILE}

rm ${TMPFILE}

