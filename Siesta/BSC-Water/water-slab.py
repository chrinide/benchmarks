#!/usr/bin/env runaiida
# -*- coding: utf-8 -*-

__copyright__ = u"Copyright (c), 2015, ECOLE POLYTECHNIQUE FEDERALE DE LAUSANNE (Theory and Simulation of Materials (THEOS) and National Centre for Computational Design and Discovery of Novel Materials (NCCR MARVEL)), Switzerland and ROBERT BOSCH LLC, USA. All rights reserved."
__license__ = "MIT license, see LICENSE.txt file"
__version__ = "0.7.0"
__contributors__ = "Andrea Cepellotti, Victor Garcia-Suarez, Alberto Garcia"

import sys
import os

from aiida.common.example_helpers import test_and_get_code
from aiida.common.exceptions import NotExistent
import argparse

def parser_setup():
    """Setup the parser of command line arguments and return it. This is
    separated from the main execution body to allow tests to
    effectively mock the setup of the parser and the command line
    arguments

    """
    parser = argparse.ArgumentParser(
        description='Run the Water benchmark',
    )
    parser.add_argument(
        '-c', type=str, required=True, dest='codename',
        help='the name of the AiiDA code that references Siesta.siesta plugin'
    )
    parser.add_argument(
        '-b', type=str, required=True, dest='basis_id', default='dzp', 
        help='the code for basis set ("sz","dzp","tzp","qzdp") (default: %(default)s)'
    )
    parser.add_argument(
        '-n', type=int, required=False, dest='mpi_nodes', default=2,
        help='the number of MPI nodes (default: %(default)s)'
    )
    parser.add_argument(
        '-s', type=int, required=False, dest='nsize', default=1,
        help='the size of the X and Y supercell edges (Z is kept constant)'
    )
    parser.add_argument(
        '-e', type=float, required=False, dest='mesh_cutoff', default=200,
        help='the mesh cutoff in Rydbergs'
    )
    parser.add_argument(
        '-r', type=int, required=False, dest='send_calc', default=0,
        help='Whether to submit (run) after build test (0: no, 1: yes) default:0 (dont-send)'
    )

    return parser

def execute(args):
    """
    The main execution of the script, which will run some preliminary checks on the command
    line arguments before passing them to the workchain and running it
    """

    ################################################################

    PsfData = DataFactory('siesta.psf')
    ParameterData = DataFactory('parameter')
    KpointsData = DataFactory('array.kpoints')
    StructureData = DataFactory('structure')

    submit_test = (args.send_calc == 0)

    # If True, load the pseudos from the family specified below
    # Use 'verdi data psf uploadfamily /path/to/water/psf_files Water "Psfs for Water Benchmark" '
    # before running the benchmark
    # Make sure you have at least version 0.11.6 of the aiida-siesta package

    auto_pseudos = True
    pseudo_family = 'Water'

    queue = None
    settings = None

    code = test_and_get_code(args.codename, expected_code_type='siesta.siesta')

    alat = 12.415 # angstrom
    # Note slab configuration, with vacuum along Z
    cell = [[alat, 0., 0.,],
            [0., alat, 0.,],
            [0., 0., 3*alat,],
           ]

    s = StructureData(cell=cell)
    #
    s.append_atom(position=(9.265348,-0.311591,0.093457),symbols=["O"])
    s.append_atom(position=(8.689128,-1.017682,-0.276697),symbols=["H"])
    s.append_atom(position=(10.094662,-0.415209,-0.490469),symbols=["H"])
    s.append_atom(position=(-0.186509,6.582004,-0.182757),symbols=["O"])
    s.append_atom(position=(-0.833894,6.192611,0.412740),symbols=["H"])
    s.append_atom(position=(0.662321,6.742639,0.383563),symbols=["H"])
    s.append_atom(position=(9.823251,5.761508,0.940713),symbols=["O"])
    s.append_atom(position=(9.005571,5.923088,0.410643),symbols=["H"])
    s.append_atom(position=(9.688245,6.215045,1.827615),symbols=["H"])
    s.append_atom(position=(-2.442836,1.431261,6.134428),symbols=["O"])
    s.append_atom(position=(-2.570216,1.993350,6.960008),symbols=["H"])
    s.append_atom(position=(-3.327390,1.025212,6.021358),symbols=["H"])
    s.append_atom(position=(4.458141,4.185816,4.778531),symbols=["O"])
    s.append_atom(position=(4.851314,3.294554,4.938545),symbols=["H"])
    s.append_atom(position=(3.887862,4.344018,5.585507),symbols=["H"])
    s.append_atom(position=(-0.597655,6.232127,7.340878),symbols=["O"])
    s.append_atom(position=(-0.826922,7.172271,7.474801),symbols=["H"])
    s.append_atom(position=(-0.299193,6.016982,8.213015),symbols=["H"])
    s.append_atom(position=(-0.077175,-7.353268,-2.499970),symbols=["O"])
    s.append_atom(position=(-0.102367,-6.946210,-1.572003),symbols=["H"])
    s.append_atom(position=(-0.575046,-8.175001,-2.387484),symbols=["H"])
    s.append_atom(position=(11.468893,2.318519,9.616319),symbols=["O"])
    s.append_atom(position=(10.852941,2.716047,8.934168),symbols=["H"])
    s.append_atom(position=(11.063907,2.235016,10.502992),symbols=["H"])
    s.append_atom(position=(2.760018,7.165212,7.926405),symbols=["O"])
    s.append_atom(position=(2.217066,7.810384,7.421360),symbols=["H"])
    s.append_atom(position=(3.634080,7.204932,7.430928),symbols=["H"])
    s.append_atom(position=(3.562673,8.864698,2.284409),symbols=["O"])
    s.append_atom(position=(3.136536,7.963589,2.180763),symbols=["H"])
    s.append_atom(position=(2.715239,9.401321,2.433852),symbols=["H"])
    s.append_atom(position=(-6.322921,5.921293,3.168741),symbols=["O"])
    s.append_atom(position=(-6.708150,5.276245,3.828675),symbols=["H"])
    s.append_atom(position=(-6.193901,5.274878,2.472903),symbols=["H"])
    s.append_atom(position=(-5.120290,6.046495,6.467963),symbols=["O"])
    s.append_atom(position=(-4.813861,5.289337,5.883631),symbols=["H"])
    s.append_atom(position=(-5.919844,5.779387,6.919640),symbols=["H"])
    s.append_atom(position=(-3.719169,-2.274022,-4.553038),symbols=["O"])
    s.append_atom(position=(-3.803478,-1.314083,-4.281183),symbols=["H"])
    s.append_atom(position=(-3.775642,-2.803014,-3.696993),symbols=["H"])
    s.append_atom(position=(6.946214,10.454103,11.284694),symbols=["O"])
    s.append_atom(position=(6.025828,10.149161,11.480295),symbols=["H"])
    s.append_atom(position=(6.726974,11.294442,10.815594),symbols=["H"])
    s.append_atom(position=(-9.631798,7.600359,10.643710),symbols=["O"])
    s.append_atom(position=(-9.618564,7.565593,9.656493),symbols=["H"])
    s.append_atom(position=(-9.991817,6.675487,10.772504),symbols=["H"])
    s.append_atom(position=(3.417795,16.635001,2.219916),symbols=["O"])
    s.append_atom(position=(3.804844,16.309335,3.102406),symbols=["H"])
    s.append_atom(position=(2.588148,16.154913,2.096074),symbols=["H"])
    s.append_atom(position=(4.859717,3.212405,11.042278),symbols=["O"])
    s.append_atom(position=(5.275413,3.658437,11.813463),symbols=["H"])
    s.append_atom(position=(5.014731,3.661887,10.207475),symbols=["H"])
    s.append_atom(position=(8.244822,12.936017,9.064412),symbols=["O"])
    s.append_atom(position=(8.420791,13.467871,8.249758),symbols=["H"])
    s.append_atom(position=(7.366755,13.226371,9.405676),symbols=["H"])
    s.append_atom(position=(9.579597,0.278024,2.890427),symbols=["O"])
    s.append_atom(position=(8.658422,0.017985,3.044218),symbols=["H"])
    s.append_atom(position=(9.659477,-0.003575,1.937320),symbols=["H"])
    s.append_atom(position=(8.187774,2.381067,12.230150),symbols=["O"])
    s.append_atom(position=(8.255311,1.377479,12.258678),symbols=["H"])
    s.append_atom(position=(9.185591,2.564351,12.331132),symbols=["H"])
    s.append_atom(position=(-1.534368,2.917604,0.267427),symbols=["O"])
    s.append_atom(position=(-0.602296,2.799934,0.582749),symbols=["H"])
    s.append_atom(position=(-1.875529,3.627059,0.810495),symbols=["H"])
    s.append_atom(position=(-0.880363,-0.698241,10.336676),symbols=["O"])
    s.append_atom(position=(-0.958528,0.286416,10.268502),symbols=["H"])
    s.append_atom(position=(-0.425531,-0.841259,11.250644),symbols=["H"])
    s.append_atom(position=(1.264053,15.405705,4.711459),symbols=["O"])
    s.append_atom(position=(0.281563,15.451274,4.453113),symbols=["H"])
    s.append_atom(position=(1.678608,14.866884,4.042833),symbols=["H"])
    s.append_atom(position=(1.688574,10.305224,9.713807),symbols=["O"])
    s.append_atom(position=(0.701869,10.255154,9.514383),symbols=["H"])
    s.append_atom(position=(1.977592,9.498760,9.291530),symbols=["H"])
    s.append_atom(position=(1.672438,6.594637,2.044788),symbols=["O"])
    s.append_atom(position=(1.132679,6.872079,2.772216),symbols=["H"])
    s.append_atom(position=(2.226379,5.798859,2.183180),symbols=["H"])
    s.append_atom(position=(5.191914,13.019778,1.983388),symbols=["O"])
    s.append_atom(position=(5.907453,12.455047,2.271780),symbols=["H"])
    s.append_atom(position=(5.151714,13.137738,1.048689),symbols=["H"])
    s.append_atom(position=(-0.846686,11.561422,4.287143),symbols=["O"])
    s.append_atom(position=(-1.684459,11.886573,3.873821),symbols=["H"])
    s.append_atom(position=(-0.567268,12.338898,4.866773),symbols=["H"])
    s.append_atom(position=(1.727755,9.262576,6.322208),symbols=["O"])
    s.append_atom(position=(1.076237,9.242472,5.594289),symbols=["H"])
    s.append_atom(position=(1.478042,10.077342,6.833535),symbols=["H"])
    s.append_atom(position=(0.102101,7.466316,4.317890),symbols=["O"])
    s.append_atom(position=(-0.598917,7.910676,3.796314),symbols=["H"])
    s.append_atom(position=(-0.363667,6.963316,5.033240),symbols=["H"])
    s.append_atom(position=(10.712179,-3.191991,2.083698),symbols=["O"])
    s.append_atom(position=(10.259476,-2.461744,1.590614),symbols=["H"])
    s.append_atom(position=(9.949089,-3.781713,2.288819),symbols=["H"])
    s.append_atom(position=(5.151062,13.981705,7.029790),symbols=["O"])
    s.append_atom(position=(5.988459,13.428922,6.911715),symbols=["H"])
    s.append_atom(position=(4.764904,13.672201,7.866964),symbols=["H"])
    s.append_atom(position=(-6.845646,4.373061,8.481353),symbols=["O"])
    s.append_atom(position=(-5.943112,4.008838,8.664561),symbols=["H"])
    s.append_atom(position=(-7.297007,3.892976,7.791411),symbols=["H"])
    s.append_atom(position=(2.456891,4.202281,6.977752),symbols=["O"])
    s.append_atom(position=(2.317176,5.142812,7.173216),symbols=["H"])
    s.append_atom(position=(1.915610,3.852743,6.256085),symbols=["H"])
    s.append_atom(position=(3.043159,0.551483,12.171976),symbols=["O"])
    s.append_atom(position=(2.985091,0.290081,11.219726),symbols=["H"])
    s.append_atom(position=(3.112921,-0.288817,12.619279),symbols=["H"])
    s.append_atom(position=(-3.332865,7.191603,3.615132),symbols=["O"])
    s.append_atom(position=(-4.291601,7.380668,3.707421),symbols=["H"])
    s.append_atom(position=(-3.038026,7.959448,4.193507),symbols=["H"])
    s.append_atom(position=(1.593772,10.923987,2.560072),symbols=["O"])
    s.append_atom(position=(1.769977,11.895889,2.478219),symbols=["H"])
    s.append_atom(position=(0.739526,10.874809,3.055950),symbols=["H"])
    s.append_atom(position=(-1.400723,2.766343,4.058999),symbols=["O"])
    s.append_atom(position=(-1.616201,2.200419,3.259730),symbols=["H"])
    s.append_atom(position=(-1.804792,2.244685,4.853950),symbols=["H"])
    s.append_atom(position=(7.582102,12.715877,5.671841),symbols=["O"])
    s.append_atom(position=(7.609837,11.805246,5.340965),symbols=["H"])
    s.append_atom(position=(7.158135,13.313100,5.050150),symbols=["H"])
    s.append_atom(position=(-7.263026,6.739839,-0.219390),symbols=["O"])
    s.append_atom(position=(-7.494997,6.065105,0.476209),symbols=["H"])
    s.append_atom(position=(-8.058064,6.791474,-0.827072),symbols=["H"])
    s.append_atom(position=(1.763174,2.684432,9.121978),symbols=["O"])
    s.append_atom(position=(0.807506,2.469325,9.195483),symbols=["H"])
    s.append_atom(position=(1.890760,3.283562,8.344842),symbols=["H"])
    s.append_atom(position=(3.370903,0.190390,9.409713),symbols=["O"])
    s.append_atom(position=(2.740041,0.903235,9.209234),symbols=["H"])
    s.append_atom(position=(2.820477,-0.609871,9.339812),symbols=["H"])
    s.append_atom(position=(-1.419382,8.720389,-0.943922),symbols=["O"])
    s.append_atom(position=(-0.959914,9.503177,-0.669325),symbols=["H"])
    s.append_atom(position=(-0.957336,7.914546,-0.605197),symbols=["H"])
    s.append_atom(position=(4.191282,9.834120,-0.518282),symbols=["O"])
    s.append_atom(position=(3.972215,9.378784,0.346213),symbols=["H"])
    s.append_atom(position=(3.724494,9.208573,-1.106304),symbols=["H"])
    s.append_atom(position=(0.923928,0.796347,6.751846),symbols=["O"])
    s.append_atom(position=(1.166703,1.125478,7.638407),symbols=["H"])
    s.append_atom(position=(1.055721,1.574699,6.168401),symbols=["H"])
    s.append_atom(position=(7.847064,6.244430,-0.993313),symbols=["O"])
    s.append_atom(position=(8.020610,7.013479,-1.629460),symbols=["H"])
    s.append_atom(position=(6.873761,6.282061,-0.845668),symbols=["H"])
    s.append_atom(position=(0.571944,10.468234,-0.074263),symbols=["O"])
    s.append_atom(position=(1.042750,10.621121,0.764649),symbols=["H"])
    s.append_atom(position=(1.291221,10.196305,-0.768775),symbols=["H"])
    s.append_atom(position=(2.328389,4.854423,11.340343),symbols=["O"])
    s.append_atom(position=(1.817269,4.533075,10.523140),symbols=["H"])
    s.append_atom(position=(3.214166,4.475896,11.239877),symbols=["H"])
    s.append_atom(position=(-3.556305,8.516287,10.121910),symbols=["O"])
    s.append_atom(position=(-2.730870,8.610466,10.685560),symbols=["H"])
    s.append_atom(position=(-4.245438,9.070806,10.529998),symbols=["H"])
    s.append_atom(position=(8.057349,4.062904,9.793328),symbols=["O"])
    s.append_atom(position=(8.047697,4.881552,10.342473),symbols=["H"])
    s.append_atom(position=(7.995173,3.301418,10.405596),symbols=["H"])
    s.append_atom(position=(5.830706,0.766867,-1.704360),symbols=["O"])
    s.append_atom(position=(5.473521,1.709640,-1.513431),symbols=["H"])
    s.append_atom(position=(5.126410,0.417470,-2.256789),symbols=["H"])
    s.append_atom(position=(6.426754,2.222080,4.197795),symbols=["O"])
    s.append_atom(position=(6.090621,1.750388,3.374708),symbols=["H"])
    s.append_atom(position=(7.131513,2.828305,3.894349),symbols=["H"])
    s.append_atom(position=(5.750188,10.685789,-4.408755),symbols=["O"])
    s.append_atom(position=(6.700097,10.548744,-4.433145),symbols=["H"])
    s.append_atom(position=(5.481959,9.977626,-5.001041),symbols=["H"])
    s.append_atom(position=(1.324222,2.764812,13.182733),symbols=["O"])
    s.append_atom(position=(1.826098,2.043714,12.791821),symbols=["H"])
    s.append_atom(position=(1.596182,3.480061,12.549313),symbols=["H"])
    s.append_atom(position=(-7.542786,7.924839,6.590549),symbols=["O"])
    s.append_atom(position=(-6.710286,7.408384,6.575713),symbols=["H"])
    s.append_atom(position=(-7.631868,8.490975,5.772782),symbols=["H"])
    s.append_atom(position=(2.558992,1.087311,3.043844),symbols=["O"])
    s.append_atom(position=(3.267060,1.077865,2.370797),symbols=["H"])
    s.append_atom(position=(3.032887,0.794970,3.812123),symbols=["H"])
    s.append_atom(position=(-5.414478,11.197387,2.828473),symbols=["O"])
    s.append_atom(position=(-5.852226,10.904357,3.670184),symbols=["H"])
    s.append_atom(position=(-5.344760,10.370074,2.258888),symbols=["H"])
    s.append_atom(position=(-3.391930,-2.484001,5.256467),symbols=["O"])
    s.append_atom(position=(-3.591513,-2.635118,6.282943),symbols=["H"])
    s.append_atom(position=(-2.640080,-1.892325,5.183117),symbols=["H"])
    s.append_atom(position=(3.487936,12.418032,5.371825),symbols=["O"])
    s.append_atom(position=(4.026530,13.120770,5.895620),symbols=["H"])
    s.append_atom(position=(2.687432,12.427160,5.857772),symbols=["H"])
    s.append_atom(position=(6.016457,3.866370,13.299842),symbols=["O"])
    s.append_atom(position=(5.364922,3.581241,13.999025),symbols=["H"])
    s.append_atom(position=(6.871827,3.437948,13.442809),symbols=["H"])
    s.append_atom(position=(-0.636421,9.211457,-3.945444),symbols=["O"])
    s.append_atom(position=(-0.754248,9.036176,-2.985201),symbols=["H"])
    s.append_atom(position=(-1.310741,9.931921,-4.088720),symbols=["H"])
    s.append_atom(position=(-4.147372,-8.361822,4.033122),symbols=["O"])
    s.append_atom(position=(-4.455221,-7.460629,4.011758),symbols=["H"])
    s.append_atom(position=(-3.170429,-8.360849,4.059559),symbols=["H"])
    s.append_atom(position=(-2.464255,3.779753,-4.588124),symbols=["O"])
    s.append_atom(position=(-3.167049,3.855773,-3.897896),symbols=["H"])
    s.append_atom(position=(-2.028129,4.616703,-4.850558),symbols=["H"])
    s.append_atom(position=(6.805011,8.374290,1.785639),symbols=["O"])
    s.append_atom(position=(6.678141,8.116117,0.844889),symbols=["H"])
    s.append_atom(position=(5.905570,8.196827,2.206741),symbols=["H"])
    s.append_atom(position=(4.817999,10.048475,4.614015),symbols=["O"])
    s.append_atom(position=(4.397308,9.437490,3.934904),symbols=["H"])
    s.append_atom(position=(4.162866,10.728503,4.826966),symbols=["H"])


    elements = list(s.get_symbols_set())

    if auto_pseudos:
        valid_pseudo_groups = PsfData.get_psf_groups(filter_elements=elements)

        try:
            PsfData.get_psf_group(pseudo_family)
        except NotExistent:
            print >> sys.stderr, "pseudo_family='{}',".format(pseudo_family)
            print >> sys.stderr, "but no group with such a name found in the DB."
            print >> sys.stderr, "Valid PSF groups are:"
            print >> sys.stderr, ",".join(i.name for i in valid_pseudo_groups)
            sys.exit(1)

    cutoff = "{} Ry".format(args.mesh_cutoff)
    n = args.nsize  # Supercell
    
    parameters = ParameterData(dict={
                    'xc-functional': 'LDA',
                    'xc-authors': 'CA',
                    'max-scfiterations': 1,
                    'scf-mix': 'density',
                    'scf-must-converge': False,
                    'dm-numberpulay': 4,
                    'dm-mixingweight': 0.3,
                    'dm-tolerance': 1.e-3,
                    'mesh-cutoff': cutoff,
                    'Solution-method': 'diagon',
                    'electronic-temperature': '25 meV',
                    'md-typeofrun': 'cg',
                    'md-numcgsteps': 0,
                    'md-maxcgdispl': '0.1 Ang',
                    'md-maxforcetol': '0.04 eV/Ang',
                    'xml-write': True,
                    '%block supercell':"\n {} 0 0 \n 0 {} 0 \n 0 0 1".format(n,n)
                    })
    #
    #
    basis_default = ParameterData(dict={
    'pao-energy-shift': '300 meV',
    '%block pao-basis-sizes': """
    O DZP                    ,
    H DZP                    """,
    })
    #
    basis_sz = ParameterData(dict={
        'PAO-BasisSize':    'SZ',
        'PAO-EnergyShift':  '50 meV'
    })
    #
    #
    basis_dzp = ParameterData(dict={
    '%block pao-basis': """
    O    3     -0.24233
    n=2   0   2   E    23.36061     3.39721
         4.50769     2.64066
         1.00000     1.00000
    n=2   1   2   E     2.78334     5.14253
         6.14996     2.59356
         1.00000     1.00000
    n=3   2   1   E    63.98188     0.16104
         3.54403
         1.00000
    H    2      0.46527
    n=1   0   2   E    99.93138     2.59932
         4.20357     1.84463
         1.00000     1.00000
    n=2   1   1   E    24.56504     2.20231
         3.52816
         1.00000        """
    })
    #
    basis_tzp = ParameterData(dict={
    '%block pao-basis': """
    O 3 -0.18361
    n=2 0 3 E 50.06241 5.0
    7.0 4.0 2.4
    1.00000 1.00000 1.0
    n=2 1 3 E 10.0 6.0
    7.0 4.0 2.2
    1.00000 1.00000 1.0
    n=3 2 1 E 50.0 0.0
    6.0
    1.00000
    H 2 0.94703
    n=1 0 3 E 50.0 6.0
    7.0 4.0 2.0
    1.00000 1.00000 1.0
    n=2 1 1 E 1000.0 0.0
    6.0
    1.00000   """
    })
    # QZDPQ-8.5 basis set
    basis_qzdp = ParameterData(dict={
    '%block pao-basis': """
    O 3
    n=2 0 4 E 50. 7.5
        8.0 5.0 3.5 2.0
    n=2 1 4 E 10. 8.3
        8.5 5.0 3.5 2.0
    n=3 2 2 E 40. 8.3 Q 6.
        8.5 2.2
    H 2
    n=1 0 4 E 50. 8.3
        8.5 5.0 3.5 2.0
    n=2 1 2 E 20. 7.8 Q 3.5
        8.0 2.0   """
    })
    #

    basis_protocols = {
        "default": basis_default,
        "sz": basis_sz,
        "dzp": basis_dzp,
        "tzp": basis_tzp,
        "qzdp": basis_qzdp
    }

    calc = code.new_calc()
    calc.label = "Water slab"
    calc.description = "Test calculation with the Siesta code. Water slab"
    calc.set_max_wallclock_seconds(180*60) # 3 hours

    #
    calc.set_resources({"num_machines": 1, "num_mpiprocs_per_machine": args.mpi_nodes})
    calc.use_structure(s)
    calc.use_parameters(parameters)
    #
    basis = basis_protocols[args.basis_id]
    calc.use_basis(basis)

    if auto_pseudos:
        try:
            calc.use_pseudos_from_family(pseudo_family)
            print "Pseudos successfully loaded from family {}".format(pseudo_family)
        except NotExistent:
            print ("Pseudo family not found. Remember to upload the "
                   "pseudo family")
            raise

    if settings is not None:
        calc.use_settings(settings)

    if submit_test:
        subfolder, script_filename = calc.submit_test()
        print "Test_submit for calculation (uuid='{}')".format(
            calc.uuid)
        print "Submit file in {}".format(os.path.join(
            os.path.relpath(subfolder.abspath),
            script_filename
            ))
    else:
        calc.store_all()
        print "created calculation; calc=Calculation(uuid='{}') # ID={}".format(
            calc.uuid,calc.dbnode.pk)
        calc.submit()
        print "submitted calculation; calc=Calculation(uuid='{}') # ID={}".format(
            calc.uuid,calc.dbnode.pk)

def main():
    """Setup the parser to retrieve the command line arguments and pass
    them to the main execution function.
    """
    parser = parser_setup()
    args   = parser.parse_args()
    result = execute(args)

if __name__ == "__main__":
    main()
