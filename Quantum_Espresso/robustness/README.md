## Robustness benchmark 

Contents: three medium-size tests of the various diagonalization algorithms

* ausurf.sh
* MOF.sh
* Si255.sh

Two small-size tests that occasionally crash

* small1.sh
* small2.sh

Pseudopotentials for all tests are in pseudo/
