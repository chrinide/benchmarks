
For compilation instructions plese see: [autoconf instructions](https://gitlab.com/QEF/q-e/-/wikis/Developers/Make-build-system)
or [CMake instructions](https://gitlab.com/QEF/q-e/-/wikis/Developers/CMake-build-system). 


Inputs are named `*.in`. One needs to copy the input file and the pseudopotential files in the execution directory. 


To run the code simply 


`mpirun [-np nnodes] cp.x -i cp_water.in  >  output_name` 

