#!/bin/bash
#SBATCH --account=prcoe07
#SBATCH --nodes=600
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=24
#SBATCH --mem=87GB
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=04:00:00
#SBATCH --partition=batch

module load Intel/2021.4.0
module load IntelMPI/2021.4.0
module load imkl/2021.4.0 

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}

export PATH=/p/project/prcoe07/nicola/src/2022/yambo-bugfixes-CPU-Intel/bin:$PATH

J_LABEL=${SLURM_NTASKS}mpi
J_SUFF="_${SLURM_CPUS_PER_TASK}thr"

srun -n ${SLURM_NTASKS} --cpu-bind=cores -m block:block \
     yambo -F yambo_${J_LABEL}.in -J ${J_LABEL}${J_SUFF}.out
