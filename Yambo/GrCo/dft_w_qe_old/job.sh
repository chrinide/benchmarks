#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --gpus-per-node=4
#SBATCH --partition=m100_usr_prod
##SBATCH --qos=m100_qos_bprod
#SBATCH --qos=m100_qos_dbg
#SBATCH --time=0:30:00
#SBATCH --account=IscrC_ProBott2_1
#SBATCH --mem=230000MB
#SBATCH --job-name=rutile
#SBATCH --error=err.job-%j
#SBATCH --output=out.job-%j
#SBATCH --mail-type=ALL
#SBATCH --mail-user=nicola.spallanzani@nano.cnr.it

module purge
module load profile/chem-phys
module load autoload qe-gpu/6.8

export OMP_NUM_THREADS=8

mpirun -np ${SLURM_NTASKS} --map-by socket:PE=8 --rank-by core \
       pw.x < scf.in > scf.out.${SLURM_JOBID}
mpirun -np ${SLURM_NTASKS} --map-by socket:PE=8 --rank-by core \
       pw.x < nscf.in > nscf.out.${SLURM_JOBID}

module purge
module load profile/chem-phys
module load autoload yambo/5.0.0

export OMP_NUM_THREADS=8
cd out/4l_k12_e80_b1000.save

mpirun -np ${SLURM_NTASKS} --map-by socket:PE=8 --rank-by core p2y
mpirun -np ${SLURM_NTASKS} --map-by socket:PE=8 --rank-by core yambo
