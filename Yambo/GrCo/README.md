# GrCo benchmark

Calculation of quasi-particle corrections on a graphene/Co interface (GrCo) composed by a graphene sheet adsorbed on a Co slab 4 layers thick, and a vacuum layer as large as the Co slab. The test represents a prototype calculation, as it involves the evaluation of a response function, of the Hartree-Fock self-energy and, finally, of the correlation part of the self-energy. The scalability and relative efficiency are reported in the Figure as a function of the number of GPU cards and show a very good scalability up to 1440 GPUs (360 nodes on Juwels-Booster@JSC, 4 NVIDIA A100 per node).

## New benchmarks
The following benchmarks are obtained starting form QE DFT calculations, the input file of QE are available in the directory `dft_w_qe`:

```
develop-v5.1.1-rev21181-hash4394c0edc-results-at-JuwelsBooster
bugfixes-v5.1.0-rev21602-hashde8dffd-strongscaling-at-JuwelsCluster
```

## Old Benchmerks
The following benchmarks are obtained starting form QE DFT calculations, the input file of QE are available in the directory `dft_w_qe_old`:

```
develop-v5.1.0-rev20475-hash9b099404b-results-at-JuwelsBooster
develop-v5.1.0-rev20475-hash9b099404b-results-at-JuwelsBooster-weakscaling
develop-v5.1.0-rev20389-hash3f02bad3e-strongscaling-at-Marconi100
develop-v5.1.0-rev20389-hash3f02bad3e-weakscaling-at-Marconi100
```
