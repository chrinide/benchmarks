#!/bin/bash
#SBATCH --account=prcoe07
#SBATCH --nodes=36
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=24
#SBATCH --gres=gpu:4
#SBATCH --mem=450GB
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=00:10:00
#SBATCH --partition=booster

module load NVHPC/21.9-GCC-10.3.0 ParaStationMPI/5.4.10-1
#export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
export OMP_NUM_THREADS=8

export PATH=/p/project/prcoe07/nicola/src/yambo-devel/bin:/p/scratch/prcoe07/opt/qe-6.8/bin:$PATH

J_LABEL=${SLURM_JOB_NUM_NODES}nodes
J_SUFF="_c_20Ry"

export CUDA_VISIBLE_DEVICES=0,1,2,3

srun -n ${SLURM_NTASKS} \
     yambo -F yambo_${J_LABEL}.in -J ${J_LABEL}${J_SUFF}.out
