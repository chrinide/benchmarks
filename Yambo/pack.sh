#!/bin/bash

list=`find . -name LOG`

for item in $list 
do
    dir=`echo $item | sed 's/LOG//'`
    echo "Packing DIR = $dir"
    cd $dir
    if [ -d ./LOG ] ; then
       tar cfz ./LOG.tgz LOG 
       rm -rf ./LOG
       #test -e ./LOG.tgz && git add ./LOG.tgz 
       echo "   ./LOG.tgz created"
    fi
    cd -
done

