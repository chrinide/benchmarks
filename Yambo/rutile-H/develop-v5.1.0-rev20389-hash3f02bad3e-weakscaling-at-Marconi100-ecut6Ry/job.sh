#!/bin/bash
#SBATCH --nodes=18
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --gpus-per-node=4
#SBATCH --partition=m100_usr_prod
#SBATCH --qos=m100_qos_bprod
##SBATCH --qos=m100_qos_dbg
#SBATCH --time=0:30:00
#SBATCH --account=IscrC_TiO2-HYD
#SBATCH --mem=230000MB
#SBATCH --job-name=rutile
#SBATCH --error=err.job-%j
#SBATCH --output=out.job-%j
#SBATCH --mail-type=ALL
#SBATCH --mail-user=nicola.spallanzani@nano.cnr.it

module purge
module load hpc-sdk/2021--binary
module load spectrum_mpi/10.4.0--binary
module load cuda/11.3

EXTLIBS=/m100/home/userexternal/nspallan/opt/devel-ext-libs-nvhpc21-spectrum10.4/pgi/mpipgifort
YAMBO_HOME=/m100/home/userexternal/nspallan/src/yambo-devel
YAMBO_BIN=${YAMBO_HOME}/bin
YAMBO_LIB=${EXTLIBS}/lib
YAMBO_V4LIB=${EXTLIBS}/v4/serial/lib

export LD_LIBRARY_PATH=$YAMBO_LIB:$YAMBO_V4LIB:$LD_LIBRARY_PATH
export PATH=$YAMBO_BIN:$PATH
export OMP_NUM_THREADS=8

J_LABEL=rutile_${SLURM_JOB_NUM_NODES}
J_SUFF=""

mpirun --map-by socket:PE=8 --rank-by core -np ${SLURM_NTASKS} \
       ${YAMBO_BIN}/yambo -F yambo_${J_LABEL}.in -J ${J_LABEL}nodes${J_SUFF}.out
