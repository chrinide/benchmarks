#! /usr/bin/env python3

import sys
import matplotlib.pyplot as plt
#import numpy as np
import pandas as pd

color1='#112F41'
color2='#2EA6A6'
color3='#47AB6C'
color4='#F28F38'
color5='#D90718'

for filename in sys.argv[2:]:

   print(filename)

#   fl = open(filename+'.dat', "r")
#   scaling = np.genfromtxt(fl, delimiter=None, autostrip=True, unpack=True, names=True)
   scaling = pd.read_csv(filename+'.dat', delim_whitespace=True)
   scaling['nodes'] = scaling['MPI']//4
   residue=scaling['WALL_TIME']-scaling['dip']-scaling['Xo']-scaling['X']-scaling['Sgm_x']-scaling['Sgm_c']
   sigma = scaling['Sgm_c']+scaling['Sgm_x']

   plt.figure(figsize=(10, 7))
  
   if sys.argv[1]=="-n":

      plt.bar(scaling['nodes'],scaling['dip']  ,width=4,color=color2,label='Dipoles')
      plt.bar(scaling['nodes'],scaling['Xo']   ,width=4,color=color4,label='Xo', bottom=scaling['dip'])
      plt.bar(scaling['nodes'],scaling['X']    ,width=4,color=color3,label='X',bottom=scaling['dip']+scaling['Xo'] )
      plt.bar(scaling['nodes'],sigma               ,width=4,color=color1,label='Self energy',bottom=scaling['dip']+scaling['Xo']+scaling['X'])
      plt.bar(scaling['nodes'],residue             ,width=4,color=color5,label='Other',bottom=scaling['dip']+scaling['Xo']+scaling['X']+sigma)


      plt.xticks(scaling['nodes'])
      plt.xlabel('#nodes',fontsize=20)

   if sys.argv[1]=="-m":

      plt.bar(scaling['MPI'],scaling['dip']  ,width=50,color=color2,label='Dipoles')
      plt.bar(scaling['MPI'],scaling['Xo']   ,width=50,color=color4,label='Xo', bottom=scaling['dip'])
      plt.bar(scaling['MPI'],scaling['X']    ,width=50,color=color3,label='X',bottom=scaling['dip']+scaling['Xo'] )
      plt.bar(scaling['MPI'],sigma               ,width=50,color=color1,label='Self energy',bottom=scaling['dip']+scaling['Xo']+scaling['X'])
      plt.bar(scaling['MPI'],residue             ,width=50,color=color5,label='Other',bottom=scaling['dip']+scaling['Xo']+scaling['X']+sigma)


      plt.xticks(scaling['MPI'])
      plt.xlabel('#MPI tasks',fontsize=20)

   if sys.argv[1]=="-t":

      plt.bar(scaling['threads'],scaling['dip']  ,width=1.3,color=color2,label='Dipoles')
      plt.bar(scaling['threads'],scaling['Xo']   ,width=1.3,color=color4,label='Xo', bottom=scaling['dip'])
      plt.bar(scaling['threads'],scaling['X']    ,width=1.3,color=color3,label='X',bottom=scaling['dip']+scaling['Xo'] )
      plt.bar(scaling['threads'],sigma               ,width=1.3,color=color1,label='Self energy',bottom=scaling['dip']+scaling['Xo']+scaling['X'])
      plt.bar(scaling['threads'],residue             ,width=1.3,color=color5,label='Other',bottom=scaling['dip']+scaling['Xo']+scaling['X']+sigma)


      plt.xticks(scaling['threads'])
      plt.xlabel('#Threads',fontsize=20)


   plt.legend( )
   plt.ylabel('Time (sec)',fontsize=20)
   plt.tick_params(axis = 'both', which = 'major', labelsize = 18)

   plt.title('Scaling - ecut=10Ry', fontsize=20)
   plt.legend(fontsize='18',framealpha=0.0)

   plt.savefig(filename+'.pdf')


plt.show()
