rm -fr README_FIX

ydb.pl -p README_FIX -i README_FIX.dat -col 2,11 -title Walltime -mode p -f_y "5984*1280/x" -labels "MPI,Time"  -legend "Perfect Scaling" -grid -mode lp -s 
ydb.pl -p README_FIX -i README_FIX.dat -col 2,11  -mode p -legend "Real Scaling" -id 1 -mode lp -grid

ydb.pl -p README_FIX -i README_FIX.dat -col 2,4 -title Dipoles -mode p -f_y "1503*1280/x" -labels "MPI,Time"  -legend "Perfect Scaling" -grid -mode lp -s 
ydb.pl -p README_FIX -i README_FIX.dat -col 2,4 -mode p -legend "Real Scaling" -id 2 -mode lp -grid

ydb.pl -p README_FIX -i README_FIX.dat -col 2,5 -title X -mode p -f_y "1067*1280/x" -labels "MPI,Time"  -legend "Perfect Scaling (Xo_TOT)" -grid -mode lp -s 
ydb.pl -p README_FIX -i README_FIX.dat -col 2,5 -mode p -legend "Real Scaling (Xo_TOT)" -id 3 -mode lp -grid
ydb.pl -p README_FIX -i README_FIX.dat -col 2,6 -title X_TOT -mode p -f_y "218*1280/x" -legend "Perfect Scaling (X_TOT)" -grid -mode lp -s  -id 3
ydb.pl -p README_FIX -i README_FIX.dat -col 2,6 -mode p -legend "Real Scaling (X_TOT)" -id 3 -mode lp -grid

ydb.pl -p README_FIX -i README_FIX.dat -col 2,10 -title Sigma_c -mode p -f_y "2637*1280/x" -labels "MPI,Time"  -legend "Perfect Scaling" -grid -mode lp -s 
ydb.pl -p README_FIX -i README_FIX.dat -col 2,10 -mode p -legend "Real Scaling" -id 4 -mode lp -grid

