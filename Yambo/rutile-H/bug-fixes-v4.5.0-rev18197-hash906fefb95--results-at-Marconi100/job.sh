#!/bin/bash
#SBATCH --nodes=240
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --gres=gpu:4
##SBATCH --partition=system
#SBATCH --partition=m100_usr_prod
#SBATCH --qos=m100_qos_bprod
#SBATCH --time=1:30:00
#SBATCH --account=cin_preM100
#SBATCH --mem=230000MB
#SBATCH --job-name=rutile
#SBATCH --error=err.job-%j
#SBATCH --output=out.job-%j
#SBATCH --mail-type=ALL
#SBATCH --mail-user=nicola.spallanzani@nano.cnr.it
##SBATCH -x r[207,212,215,216,217,220,221,227,238,225,209,233,234]n[01-20]

module purge
module load autoload 
module load zlib/1.2.11--gnu--8.4.0 
module load szip/2.1.1--gnu--8.4.0
module load cuda/10.1
module load pgi/19.10--binary
module load spectrum_mpi/10.3.1--binary

OPT=/m100/home/userexternal/nspallan/opt/spectrum-10.3
EXT_LIB=$OPT/external/lib
EXT_INC=$OPT/external/include
#LAPACK_LIB=/m100/home/userexternal/nspallan/opt/pgi-19.10/lapack-3.9.0/lib
HDF5_LIB=$OPT/hdf5-1.12.0/lib
HDF5_INC=$OPT/hdf5-1.12.0/include
NETCDF_LIB=$OPT/netcdf-4.4.1.1/lib
NETCDF_INC=$OPT/netcdf-4.4.1.1/include
NETCDFF_LIB=$OPT/netcdff-4.4.4/lib
NETCDFF_INC=$OPT/netcdff-4.4.4/include
PNETCDF_LIB=$OPT/pnetcdf-1.12.1/lib
YAMBO_BIN=$OPT/yambo-devel/bin

#export LD_LIBRARY_PATH=$NETCDFF_LIB:$NETCDF_LIB:$HDF5_LIB:$EXT_LIB:$LAPACK_LIB:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$NETCDFF_LIB:$NETCDF_LIB:$HDF5_LIB:$EXT_LIB:$LD_LIBRARY_PATH
export PATH=$YAMBO_BIN:$PATH
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

J_LABEL=rutile_${SLURM_JOB_NUM_NODES}

mpirun --map-by socket:PE=8 --rank-by core \
       -np ${SLURM_NTASKS} ./gpu_bind.sh \
       ${YAMBO_BIN}/yambo -F yambo_${J_LABEL}.in -J ${J_LABEL}nodes.out
