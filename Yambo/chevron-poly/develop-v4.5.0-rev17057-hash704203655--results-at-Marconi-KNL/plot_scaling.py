#! /usr/bin/env python3

import sys
import matplotlib.pyplot as plt
import numpy as np

color1='#112F41'
color2='#2EA6A6'
color3='#47AB6C'
color4='#F28F38'
color5='#D90718'


for filename in sys.argv[2:]:

   print(filename)

   file = open(filename+'.dat', "r")
   scaling = np.genfromtxt(file, delimiter=None, autostrip=True,unpack=True,names=True)


   residue=scaling['WALL_TIME']-scaling['dip_TOT']-scaling['Xo_TOT']-scaling['X_TOT']-scaling['Sgm_x_TOT']-scaling['Sgm_c_TOT']
   sigma = scaling['Sgm_c_TOT']+scaling['Sgm_x_TOT']

   plt.figure(figsize=(10, 7))
  
   if sys.argv[1]=="-m":

      plt.bar(scaling['MPI'],scaling['dip_TOT']  ,width=400,color=color2,label='Dipoles')
      plt.bar(scaling['MPI'],scaling['Xo_TOT']   ,width=400,color=color4,label='Xo', bottom=scaling['dip_TOT'])
      plt.bar(scaling['MPI'],scaling['X_TOT']    ,width=400,color=color3,label='X',bottom=scaling['dip_TOT']+scaling['Xo_TOT'] )
      plt.bar(scaling['MPI'],sigma               ,width=400,color=color1,label='Sigma',bottom=scaling['dip_TOT']+scaling['Xo_TOT']+scaling['X_TOT'])
      plt.bar(scaling['MPI'],residue             ,width=400,color=color5,label='Other',bottom=scaling['dip_TOT']+scaling['Xo_TOT']+scaling['X_TOT']+sigma)


      plt.xticks(scaling['MPI'])
      plt.xlabel('#MPI tasks',fontsize=20)

   if sys.argv[1]=="-t":

      plt.bar(scaling['threads'],scaling['dip_TOT']  ,width=1,color=color2,label='Dipoles')
      plt.bar(scaling['threads'],scaling['Xo_TOT']   ,width=1,color=color4,label='Xo', bottom=scaling['dip_TOT'])
      plt.bar(scaling['threads'],scaling['X_TOT']    ,width=1,color=color3,label='X',bottom=scaling['dip_TOT']+scaling['Xo_TOT'] )
      plt.bar(scaling['threads'],sigma               ,width=1,color=color1,label='Sigma',bottom=scaling['dip_TOT']+scaling['Xo_TOT']+scaling['X_TOT'])
      plt.bar(scaling['threads'],residue             ,width=1,color=color5,label='Other',bottom=scaling['dip_TOT']+scaling['Xo_TOT']+scaling['X_TOT']+sigma)


      plt.xticks(scaling['threads'])
      plt.xlabel('#Threads',fontsize=20)


   plt.legend( )
   plt.ylabel('Time (sec)',fontsize=20)
   plt.tick_params(axis = 'both', which = 'major', labelsize = 18)

   #plt.title('Scaling', fontsize=20)
   plt.legend(fontsize='18',framealpha=0.0)

   plt.savefig(filename+'.pdf')


plt.show()
